const { request } = require('graphql-request');
const url = 'http://localhost:3001/graphql';


const mongoose = require('mongoose');
const express = require('express');
const graphqlHttp = require('express-graphql');

const app = express();

const graphQLSchema = require('./../graphql/schema/index');
const graphQLResolvers = require('./../graphql/resolvers/index');

let todoId;


app.use('/graphql', graphqlHttp({
    schema: graphQLSchema,
    rootValue: graphQLResolvers,
    graphiql: true
}));


const Schema = mongoose.Schema;
const ModelSchema = new Schema({
    name: String,
    priority: String,
    isDone: Boolean
});

const queryGetTodos = `
    query {
        todos {
            _id
            name
            priority
            isDone
        }
    }
`;

const mutationCreateTodo = `
    mutation {
        createTodo(todoInput: {
            name: "Send an email to Brigitte",
            priority: "medium",
            isDone: true
    }) 
    {
        name
        priority
        isDone
  }
}
`;

const model = mongoose.model('todos', ModelSchema);

const createRecordInDb = new model({
    name: 'Find a job',
    priority: 'high',
    isDone: false
});

createRecordInDb.save(function (err) {
    if (err) return handleError(err);
    console.log('Added a todo task in the test database')
});

function testGetTodos() {
    request(url, queryGetTodos)
        .then((res) => {
            const doc = res.todos[0];
            if (doc.name === 'Find a job' && doc.priority === 'high' && doc.isDone === false) {
                console.log('[Test 1 PASSED]: Create record in DB manually and query it using GraphQL');
                todoId = doc._id;
                return doc._id;
            }
            else
                console.log('[Test 1 FAILED]: Create record in DB manually and query it using GraphQL');
            // console.log(res);
        })
        .catch(console.error);
}

function testCreateTodo() {
    request(url, mutationCreateTodo)
        .then(
            request(url, queryGetTodos)
                .then((res) => {
                    const doc = res.todos[1];
                    if (doc.name === 'Send an email to Brigitte' && doc.priority === 'medium' && doc.isDone === true)
                        console.log('[Test 2 PASSED]: Create record in DB using GraphQL query and query it back using GraphQL')
                    else
                        console.log('[Test 2 FAILED]: Create record in DB using GraphQL query and query it back using GraphQL')
                })
                .catch(console.error)
        )
        .catch(console.error);
}

mongoose
    .connect('mongodb://localhost/todo-test')
    .then(() => {
        app.listen(3001);
        console.log("Created a 'todo-test' DB for testing purpose");
    })
    .then(() => {
        testGetTodos();
        testCreateTodo();
    })
    .catch(err => {
        console.log(err)
    });
