'use strict';

const fs = require('fs');
const path = require('path');
const EasyGraphQLTester = require('../node_modules/easygraphql-tester/lib');

const schemaCode = fs.readFileSync(path.join(__dirname, '..', 'graphql', 'schema', 'schema.gql'), 'utf8');

describe('Test the schema and queries', () => {
    let tester;

    before(() => {
        tester = new EasyGraphQLTester(schemaCode)
    });

    describe('Queries', () => {
        it('Invalid query getTodo', () => {
            const invalidQuery = `
                {
                  getTodo {
                    _id
                    name
                    isDone
                    }
                  }
                }
              `
            tester.test(false, invalidQuery)
        });

        it('Should pass with a valid query to retrieve all todos', () => {
            const validQuery = `
                {
                  todos {
                    _id
                    name
                    priority
                    isDone
                  }
                }
              `
            tester.test(true, validQuery)
        })
    })
});


