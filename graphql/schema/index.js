const { buildSchema } = require('graphql');

module.exports = buildSchema(
    `
        type Todo {
            _id: ID!
            name: String!
            priority: String
            isDone: Boolean!
        }
        
        type RootQuery {
            todos: [Todo!]!
        }
        
        input TodoInput {
            name: String!
            priority: String
            isDone: Boolean!
        }
        
        type RootMutation {
            createTodo(todoInput: TodoInput): Todo
            checkAsDone(_id: ID): Todo
            checkAsUndone(_id: ID): Todo
            deleteTodo(_id: ID): Todo
        }
        
        schema {
            query: RootQuery
            mutation: RootMutation
        }
    `
);