const Todo = require('../../models/todo');

module.exports = {
    todos: async () => {
        try {
            const todos = await Todo.find();
            return todos.map(todo => {
                return {...todo._doc}
            });
        }
        catch (err) {
            throw err;
        }
    },
    createTodo: async args => {
        const todo = new Todo({
            name: args.todoInput.name,
            priority: args.todoInput.priority,
            isDone: args.todoInput.isDone
        });
        try {
            const result = await todo.save();
            return {...result._doc}
        }
        catch (err) {
            throw err
        }
    },
    checkAsDone: async args => {
        try {
            const todo = await Todo.findById(args._id);
            await todo.updateOne({isDone: true});
            return {
                ...todo._doc,
                isDone: true
            }
        }
        catch (err) {
            throw err;
        }
    },
    checkAsUndone: async args => {
        try {
            const todo = await Todo.findById(args._id);
            await todo.updateOne({isDone: false});
            return {
                ...todo._doc,
                isDone: false
            }
        }
        catch (err) {
            throw err;
        }
    },
    deleteTodo: async args => {
        try {
            const todo = await Todo.findById(args._id);
            await todo.deleteOne({});
            return todo
        }
        catch (err) {
            throw err;
        }
    }
};