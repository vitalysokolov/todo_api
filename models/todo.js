const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const todoSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    priority: {
        type: String,
        required: false
    },
    isDone: {
        type: Boolean,
        required: true
    }
});

module.exports = mongoose.model('todo', todoSchema);