const express = require('express');
const graphqlHttp = require('express-graphql');
const mongoose = require('mongoose');

const graphQLSchema = require('./graphql/schema/index');
const graphQLResolvers = require('./graphql/resolvers/index');

const app = express();
const port = 3000;

app.use('/graphql', graphqlHttp({
    schema: graphQLSchema,
    rootValue: graphQLResolvers,
    graphiql: true
}));

mongoose
    .connect('mongodb://localhost/todo')
    .then(() => {
        app.listen(port);
    })
    .catch(err => {
        console.log(err)
    });

app.get('/', (req, res) => {
    res.send('Go to /graphql');
});

